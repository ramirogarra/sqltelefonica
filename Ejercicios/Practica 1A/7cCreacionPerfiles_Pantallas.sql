USE db_Ramiro;
drop table perfiles_pantallas;

create table PERFILES_PANTALLAS(
--PyP_ID integer NOT NULL PRIMARY KEY (PyP_ID) IDENTITY,
PER_ID integer FOREIGN KEY REFERENCES PERFILES (PER_ID),
PAN_ID integer FOREIGN KEY REFERENCES PANTALLAS (PAN_ID),

primary key (PER_ID,PAN_ID)
);