use db_cine_Ramiro;
drop table peliculas_actores;

create table peliculas_actores(
PEL_ID integer FOREIGN KEY REFERENCES peliculas (PEL_ID),
ACT_ID integer FOREIGN KEY REFERENCES actores (ACT_ID),

primary key (PEL_ID,ACT_ID)
);