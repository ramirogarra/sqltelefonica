use db_cine_Ramiro;
create table generos(
GEN_ID integer NOT NULL PRIMARY KEY (GEN_ID) IDENTITY,
GEN_DESCRIPTION VARCHAR(50)
);
create table peliculas(
PEL_ID integer NOT NULL PRIMARY KEY (PEL_ID) IDENTITY,
GEN_ID integer FOREIGN KEY REFERENCES generos (GEN_ID),
PEL_DESCRIPCION VARCHAR (50)
);
